﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostRegisterUserValidator : AbstractValidator<PostRegisterUserRequest>
    {
        public PostRegisterUserValidator()
        {
            RuleFor(u => u.Username).NotNull().NotEmpty();
            RuleFor(u => u.Password).NotNull().NotEmpty();
            RuleFor(u => u.Email).NotNull().NotEmpty().EmailAddress();
        }
    }
}
