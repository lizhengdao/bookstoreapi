﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PutOrderValidator : AbstractValidator<PostOrderRequest>
    {
        public PutOrderValidator()
        {
            RuleFor(o => o.UserId).NotNull();
            RuleFor(o => o.CartId).NotNull();
        }
    }
}
