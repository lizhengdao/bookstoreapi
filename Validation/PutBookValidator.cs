﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PutBookValidator : AbstractValidator<PutBookRequest>
    {
        public PutBookValidator()
        {
            RuleFor(b => b.Title).NotNull().NotEmpty();

            RuleFor(b => b.Isbn).NotNull().NotEmpty().Length(10, 13);

            RuleFor(b => b.Price).NotNull().ScalePrecision(19, 2);

            RuleFor(b => b.Description).NotNull().NotEmpty();

            RuleFor(b => b.NumberOfPages).NotNull();

            RuleFor(b => b.Stock).NotNull();

            RuleFor(b => b.Status).NotNull();

            RuleFor(b => b.PublishedAt).NotNull();
        }
    }
}
