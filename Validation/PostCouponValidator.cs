﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostCouponValidator : AbstractValidator<PostCouponRequest>
    {
        public PostCouponValidator()
        {
            RuleFor(c => c.UserId).NotNull();
            RuleFor(c => c.ProcentOff).NotNull();
            RuleFor(c => c.ValidNum).NotNull();
        }
    }
}
