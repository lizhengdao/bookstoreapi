﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostTokensValidator : AbstractValidator<PostTokensRequest>
    {
        public PostTokensValidator()
        {
            RuleFor(t => t.RefreshToken).NotEmpty().NotNull();
            RuleFor(t => t.Token).NotEmpty().NotNull();
        }
    }
}
