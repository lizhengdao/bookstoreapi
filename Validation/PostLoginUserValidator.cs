﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PostLoginUserValidator : AbstractValidator<PostUserLoginRequest>
    {
        public PostLoginUserValidator()
        {
            RuleFor(u => u.Email).NotNull().NotEmpty().EmailAddress();
            RuleFor(u => u.Password).NotNull().NotEmpty();
        }
    }
}
