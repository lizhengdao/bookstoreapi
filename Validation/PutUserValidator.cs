﻿using BooksApi.Requests;
using FluentValidation;

namespace BooksApi.Validation
{
    public class PutUserValidator : AbstractValidator<PutUserRequest>
    {
        public PutUserValidator()
        {
            RuleFor(u => u.Username).NotNull().NotEmpty();
            RuleFor(u => u.Password).NotNull().NotEmpty();
            RuleFor(u => u.Email).NotNull().NotEmpty().EmailAddress();
        }
    }
}
