﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Profiles.Resolvers;
using BooksApi.Requests;
using BooksApi.Responses;

namespace BooksApi.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<ApplicationUser, GetUserResponse>().ForMember(u => u.Avatar, opt => opt.MapFrom<AvatarResolver>());
            CreateMap<PostRegisterUserRequest, ApplicationUser>();
            CreateMap<PutUserRequest, ApplicationUser>();
        }
    }
}
