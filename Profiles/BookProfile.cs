﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Responses.Book;

namespace BooksApi.Profiles
{
    public class BookProfile : Profile
    {
        private readonly IMapper mapper;

        public BookProfile(IMapper mapper)
        {
            this.mapper = mapper;
        }
        public BookProfile()
        {
            CreateMap<Book, GetBookResponse>()
                .ForMember(gbr => gbr.Authors, opt => opt.MapFrom(b => b.AuthorBooks))
                .ForMember(gbr => gbr.Id, opt => opt.MapFrom(b => b.Id))
                .ForMember(gbr => gbr.Title, opt => opt.MapFrom(ab => ab.Title))
                .ForMember(gbr => gbr.Isbn, opt => opt.MapFrom(ab => ab.Isbn))
                .ForMember(gbr => gbr.Price, opt => opt.MapFrom(ab => ab.Price))
                .ForMember(gbr => gbr.Description, opt => opt.MapFrom(ab => ab.Description))
                .ForMember(gbr => gbr.Image, opt => opt.MapFrom(ab => ab.Image))
                .ForMember(gbr => gbr.NumberOfPages, opt => opt.MapFrom(ab => ab.NumberOfPages))
                .ForMember(gbr => gbr.Stock, opt => opt.MapFrom(ab => ab.Stock))
                .ForMember(gbr => gbr.Status, opt => opt.MapFrom(ab => ab.Status))
                .ForMember(gbr => gbr.PublishedAt, opt => opt.MapFrom(ab => ab.PublishedAt));
            CreateMap<AuthorBook, GetAuthorResponse>()
                .ForMember(gar => gar.FirstName, mapper => mapper.MapFrom(ab => ab.Author.FirstName))
                .ForMember(gar => gar.LastName, mapper => mapper.MapFrom(ab => ab.Author.LastName));
            CreateMap<Author, GetBookResponse>();
            CreateMap<PostBookRequest, Book>();
            CreateMap<PutBookRequest, Book>();
        }
    }
}
