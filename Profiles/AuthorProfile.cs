﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;

namespace BooksApi.Profiles
{
    public class AuthorProfile : Profile
    {
        public AuthorProfile()
        {
            CreateMap<Author, GetAuthorResponse>();
            CreateMap<PostAuthorRequest, Author>();
            CreateMap<PutAuthorRequest, Author>();
        }
    }
}
