﻿using AutoMapper;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Requests.CartBook;
using BooksApi.Responses;

namespace BooksApi.Profiles
{
    public class CartBookProfile : Profile
    {
        public CartBookProfile()
        {
            CreateMap<CartBook, GetCartBookResponse>();
            CreateMap<PostCartBookRequest, CartBook>();
            CreateMap<PutCartBookRequest, CartBook>();
        }
    }
}
