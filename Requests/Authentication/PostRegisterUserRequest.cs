﻿namespace BooksApi.Requests
{
    public class PostRegisterUserRequest
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}
