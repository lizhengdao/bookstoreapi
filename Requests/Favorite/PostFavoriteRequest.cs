﻿namespace BooksApi.Requests
{
    public class PostFavoriteRequest
    {
        public int? UserId { get; set; }

        public int? BookId { get; set; }
    }
}
