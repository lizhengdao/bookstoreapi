﻿namespace BooksApi.Requests.Checkout
{
    public class PostPaymentRequest
    {
        public string Token { get; set; }
    }
}
