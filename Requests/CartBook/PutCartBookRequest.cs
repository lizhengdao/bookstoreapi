﻿namespace BooksApi.Requests.CartBook
{
    public class PutCartBookRequest
    {
        public int? BookId { get; set; }

        public int? Quantity { get; set; }
    }
}
