﻿namespace BooksApi.Requests.Cart
{
    public class PutCartRequest
    {
        public int? UserId { get; set; }

        public int? CouponId { get; set; }
    }
}
