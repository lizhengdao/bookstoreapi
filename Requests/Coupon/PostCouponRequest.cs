﻿namespace BooksApi.Requests
{
    public class PostCouponRequest
    {
        public int? UserId { get; set; }

        public int? ValidNum { get; set; }

        public double? ProcentOff { get; set; }
    }
}
