﻿using BooksApi.Exceptions;
using BooksApi.Responses;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Stripe;

namespace BooksApi.Controllers
{
    [Route("api/error")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ControllerBase
    {
        public ActionResult<GetExceptionResponse> Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error;
            int statusCode = 500;
            string code = "Application error";

            if (exception is ServiceException serviceException)
            {
                statusCode = 409;
                code = serviceException.Code;
            }
            else if (exception is StripeException stripeException)
            {
                statusCode = 400;
                code = stripeException.Message;
            }

            Response.StatusCode = statusCode;

            return new GetExceptionResponse(code);
        }
    }
}
