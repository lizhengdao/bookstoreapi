﻿using BooksApi.Constants;
using BooksApi.Requests.Checkout;
using BooksApi.Responses;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    [ApiController]
    [Route("api/payment")]
    [Produces(MediaTypeNames.Application.Json)]
    [Authorize(Policy = AuthorizationPolicies.CustomerPolicy)]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentService paymentService;

        public PaymentController(PaymentService paymentService)
        {
            this.paymentService = paymentService;
        }
        /// <summary>
        /// Returns Stripe client secret.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /payment/5
        /// 
        /// </remarks>
        /// <param name="cartId">Unique id of cart.</param>
        /// <response code="200">Returns Stripe client secret.</response>
        /// <response code="409">If cart not found.</response>    
        [HttpGet("{cartId}")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetStripeResponse))]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetStripeResponse>> GetToken(int cartId)
        {
            var res = await paymentService.Payment(cartId);

            return Ok(res);
        }
        /// <summary>
        /// Returns order for which payment was confirmed.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /payment/5/confirm
        ///     {
        ///         "token": "tok_apdoiap4021"   
        ///     }
        /// 
        /// </remarks>
        /// <param name="cartId">Unique id of cart.</param>
        /// <param name="paymentRequest">Unique id of cart.</param>
        /// <response code="200">Returns if payment successs and payment order.</response>
        /// <response code="409">If cart not found or book out of stock or payment failed.</response>  
        [HttpPost("{cartId}/confirm")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(GetPaymentResponse))]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetPaymentResponse>> PostChargeInforamtion(int cartId, PostPaymentRequest paymentRequest)
        {
            var res = await paymentService.ConfirmPaymentAsync(cartId, paymentRequest);

            return Ok(res);
        }
    }
}
