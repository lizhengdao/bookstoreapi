﻿using BooksApi.Constants;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [ApiController]
    [Route("api/authenticate")]
    public class AuthenticationController : ControllerBase
    {
        private readonly AuthenticationService authenticationService;

        public AuthenticationController(AuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService;
        }
        /// <summary>
        /// Creates new user if one does not exist
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /authenticate/register
        ///     {
        ///        "username": "TestUser",
        ///        "email": "test@mail.com".
        ///        "password": "pa$$w0rd"
        ///     }
        ///
        /// </remarks>
        /// <param name="newUser">Data of a new user</param>
        /// <returns>Access and refresh token for newly created user</returns>
        /// <response code="200">Returns access and refresh token for newly created user</response>
        /// <response code="409">If user username or email in use</response>
        [HttpPost("register")]
        [SwaggerResponse(200, Type = typeof(GetAuthenticationResponse))]
        [SwaggerResponse(409, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetAuthenticationResponse>> CreateUserAsync(
            [Required] PostRegisterUserRequest newUser)
        {
            var result = await authenticationService.RegisterAsync(newUser);

            Response.Headers.Add(AuthenticationHeaders.AccessToken, result.AccessToken);
            Response.Headers.Add(AuthenticationHeaders.RefreshToken, result.RefreshToken);


            return Ok();
        }
        /// <summary>
        /// Logins user if one exist
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /authenticate/login
        ///     {
        ///        "email": "test@mail.com".
        ///        "password": "pa$$w0rd"
        ///     }
        ///
        /// </remarks>
        /// <param name="loginCredentials">User credentials for login</param>
        /// <returns>Access and refresh token for user</returns>
        /// <response code="200">Returns access and refresh token user</response>
        /// <response code="409">If user credentials are wrong</response>
        [HttpPost("login")]
        [SwaggerResponse(200, Type = typeof(GetAuthenticationResponse))]
        [SwaggerResponse(409, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetAuthenticationResponse>> LoginUserAsync(
            [Required] PostUserLoginRequest loginCredentials)
        {
            var result = await authenticationService.LoginAsync(loginCredentials);

            Response.Headers.Add(AuthenticationHeaders.AccessToken, result.AccessToken);
            Response.Headers.Add(AuthenticationHeaders.RefreshToken, result.RefreshToken);

            return Ok();
        }
        /// <summary>
        /// Refreshes user token when it expires
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /authenticate/refresh
        ///     "refresh-token" : "01293aojds0as93j2"
        ///     
        /// </remarks>
        /// <param name="refreshToken"></param>
        /// <returns>New token</returns>
        /// <response code="200">Returns new token</response>
        /// <response code="409">If user session does not exist.</response>
        [HttpPost("refresh")]
        [SwaggerResponse(200, Type = typeof(GetTokenResponse))]
        [SwaggerResponse(409, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult<GetTokenResponse>> RefreshTokenAsync(
            [FromHeader(Name = AuthenticationHeaders.RefreshToken)][Required] string refreshToken)
        {
            var result = await authenticationService.RefreshTokenAsync(refreshToken);

            Response.Headers.Add(AuthenticationHeaders.AccessToken, result.AccessToken);
            Response.Headers.Add(AuthenticationHeaders.RefreshToken, result.RefreshToken);

            return Ok();
        }
        /// <summary>
        /// Destroys user session
        /// </summary>
        /// <response code="200">Returns nothing</response>
        /// <response code="409">If user session does not exist.</response>
        [HttpPost("logout")]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status409Conflict, Type = typeof(GetExceptionResponse))]
        public async Task<ActionResult> LogoutUserAsync()
        {
            int sessionId = int.Parse(User.FindFirstValue(AccessTokenClaims.SessionId));

            await authenticationService.LogoutAsync(sessionId);

            return Ok();
        }
    }
}
