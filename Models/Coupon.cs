﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksApi.Models
{
    public class Coupon
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(User))]
        [Required]
        public int UserId { get; set; }
        public ApplicationUser User { get; set; }

        [Required]
        public int ValidNum { get; set; }
        [Required]
        public decimal ProcentOff { get; set; }
        // Default value set in context
        [Required]
        public DateTime CreatedAt { get; set; }

        public ICollection<Cart> Carts { get; set; }
    }
}
