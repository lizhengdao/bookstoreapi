﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksApi.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // Foreign key constraint specified in context
        public int UserId { get; set; }
        public ApplicationUser User { get; set; }

        [ForeignKey(nameof(Cart))]
        [Required]
        public int CartId { get; set; }
        public Cart Cart { get; set; }

        [Required]
        [Column(TypeName = "decimal(19,2)")]
        public decimal Total { get; set; }
        // Default value set in context
        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
