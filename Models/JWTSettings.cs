﻿using System;

namespace BooksApi.Models
{
    public class JwtSettings
    {
        private string issuer;

        private string secret;

        private double duration;

        public string Issuer
        {
            get => issuer;
            set => _ = value == null ? throw new ArgumentNullException() : issuer = value;
        }

        public string Secret
        {
            get => secret;
            set => _ = value == null ? throw new ArgumentNullException() : secret = value;
        }

        public double Duration
        {
            get => duration;
            set => _ = duration < 0 ? throw new ArgumentNullException() : duration = value;
        }
    }
}
