﻿using System;

namespace BooksApi.Models
{
    public class EmailMetadata
    {
        private string sender;

        private string smtpServer;

        private int port;

        private string username;

        private string password;

        public string Sender
        {
            get => sender;
            set => _ = value == null ? throw new ArgumentNullException() : sender = value;
        }

        public string SmtpServer
        {
            get => smtpServer;
            set => _ = value == null ? throw new ArgumentNullException() : smtpServer = value;
        }

        public int Port
        {
            get => port;
            set => _ = port < 0 ? throw new ArgumentNullException() : port = value;
        }

        public string Username
        {
            get => username;
            set => _ = value == null ? throw new ArgumentNullException() : username = value;
        }

        public string Password
        {
            get => password;
            set => _ = value == null ? throw new ArgumentNullException() : password = value;
        }
    }
}
