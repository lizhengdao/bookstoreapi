﻿using BooksApi.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksApi.Models
{
    public class Cart
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; private set; }
        // Foreign key relationship specified in context        
        public int UserId { get; set; }
        public ApplicationUser User { get; set; }

        [ForeignKey(nameof(Coupon))]
        public int? CouponId { get; set; }
        public Coupon Coupon { get; set; }

        // Default value set in context
        [Required]
        public DateTime CreatedAt { get; set; }
        // Default value set in context
        [Required]
        public CartStatus Status { get; set; }

        public string StripePaymentId { get; set; }

        public ICollection<CartBook> CartBooks { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
