﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace BooksApi.Models
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string Avatar { get; set; }

        public string StripeCustomerId { get; set; }

        public ICollection<Favorite> Favorites { get; set; }

        public ICollection<Cart> Carts { get; set; }

        public ICollection<Session> Sessions { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
