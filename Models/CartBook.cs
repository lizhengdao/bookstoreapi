﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BooksApi.Models
{
    public class CartBook
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(Book))]
        [Required]
        public int BookId { get; set; }
        public Book Book { get; set; }

        [ForeignKey(nameof(Cart))]
        [Required]
        public int CartId { get; set; }
        public Cart Cart { get; set; }

        [Required]
        public int Quantity { get; set; }
        // Default value set in context
        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
