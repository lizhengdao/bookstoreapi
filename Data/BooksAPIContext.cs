﻿using BooksApi.Constants;
using BooksApi.Enums;
using BooksApi.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace BooksApi.Data
{
    public class BooksApiContext : IdentityDbContext<ApplicationUser, IdentityRole<int>, int>
    {
        public BooksApiContext(DbContextOptions<BooksApiContext> options) : base(options) { }

        private void SeedRoles(ModelBuilder modelBuilder)
        {
            string[] roles = typeof(UserRoles)
                .GetFields(BindingFlags.Public | BindingFlags.Static)
                .Select(f => (string)f.GetRawConstantValue())
                .ToArray();

            for (int i = 0; i < roles.Length; i++)
            {
                modelBuilder.Entity<IdentityRole<int>>()
                    .HasData(new IdentityRole<int>
                    {
                        Id = i + 1,
                        Name = roles[i],
                        NormalizedName = roles[i].ToUpper(),
                        ConcurrencyStamp = Guid.NewGuid().ToString()
                    });
            }
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            var mockBooks = File.ReadAllText(@"Data/Seeds/mock-books.js");
            var mockAuthors = File.ReadAllText(@"Data/Seeds/mock-authors.js");
            var mockAuthorBooks = File.ReadAllText(@"Data/Seeds/mock-author-books.js");
            var mockCoupons = File.ReadAllText(@"Data/Seeds/mock-coupons.js");

            List<Book> books = JsonConvert.DeserializeObject<List<Book>>(mockBooks);
            List<Author> authors = JsonConvert.DeserializeObject<List<Author>>(mockAuthors);
            List<AuthorBook> authorBooks = JsonConvert.DeserializeObject<List<AuthorBook>>(mockAuthorBooks);
            List<Coupon> coupons = JsonConvert.DeserializeObject<List<Coupon>>(mockCoupons);

            modelBuilder.Entity<Book>().HasData(books);
            modelBuilder.Entity<Author>().HasData(authors);
            modelBuilder.Entity<AuthorBook>().HasData(authorBooks);
            modelBuilder.Entity<Coupon>().HasData(coupons);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().HasIndex(u => u.Email).IsClustered(false);
            modelBuilder.Entity<ApplicationUser>().HasIndex(u => u.StripeCustomerId).IsUnique().IsClustered(false);

            modelBuilder.Entity<Book>().HasIndex(b => b.Isbn).IsUnique().IsClustered(false);
            modelBuilder.Entity<Book>().Property(b => b.CreatedAt).HasDefaultValueSql("GETUTCDATE()");
            modelBuilder.Entity<Book>().HasIndex(b => b.Isbn).IsClustered(false);
            modelBuilder.Entity<Book>().HasIndex(b => b.Title).IsClustered(false);

            modelBuilder.Entity<Author>().HasIndex(a => new { a.FirstName, a.LastName }).IsUnique().IsClustered(false);

            modelBuilder.Entity<Favorite>().HasIndex(f => new { f.BookId, f.UserId }).IsUnique().IsClustered(false);

            modelBuilder.Entity<Coupon>().Property(c => c.CreatedAt).HasDefaultValueSql("GETUTCDATE()");

            modelBuilder.Entity<Cart>().Property(c => c.CreatedAt).HasDefaultValueSql("GETUTCDATE()");
            modelBuilder.Entity<Cart>().Property(c => c.Status).HasDefaultValue(CartStatus.Unordered);
            modelBuilder.Entity<Cart>().HasOne(c => c.User).WithMany(u => u.Carts).HasForeignKey(c => c.UserId).IsRequired().OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Order>().Property(o => o.CreatedAt).HasDefaultValueSql("GETUTCDATE()");
            modelBuilder.Entity<Order>().HasOne(o => o.User).WithMany(u => u.Orders).IsRequired().OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<CartBook>().Property(cb => cb.CreatedAt).HasDefaultValueSql("GETUTCDATE()");

            modelBuilder.Entity<Session>().Property(s => s.CreatedAt).HasDefaultValueSql("GETUTCDATE()");
            modelBuilder.Entity<Session>().HasIndex(s => s.RefreshTokenHash).IsUnique().IsClustered(false);
            // Role seeding
            SeedRoles(modelBuilder);
            // Db seeding
            SeedData(modelBuilder);
        }

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<AuthorBook> AuthorBooks { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public DbSet<Favorite> Favorites { get; set; }

        public DbSet<Coupon> Coupons { get; set; }

        public DbSet<Cart> Carts { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<CartBook> CartBooks { get; set; }

        public DbSet<Session> Sessions { get; set; }
    }
}
