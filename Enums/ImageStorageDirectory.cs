﻿namespace BooksApi.Enums
{
    public enum ImageStorageDirectory
    {
        Avatars, Books
    }
}
