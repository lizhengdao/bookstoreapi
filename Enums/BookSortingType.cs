﻿namespace BooksApi.Models
{
    public enum BookSortingType
    {
        Asc, Desc
    }
}
