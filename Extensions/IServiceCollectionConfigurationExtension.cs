﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BooksApi.Extensions
{
    public static class IServiceCollectionConfigurationExtension
    {
        public static IServiceCollection AddSingletonConfigurationModel<TModel>(
            this IServiceCollection services,
            IConfigurationSection section)
            where TModel : class
        {
            return services.AddSingleton(section.Get<TModel>());
        }
    }
}
