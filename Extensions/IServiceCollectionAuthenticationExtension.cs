﻿using BooksApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using System.Threading.Tasks;

namespace BooksApi.Extensions
{
    public static class IServiceCollectionAuthenticationExtension
    {
        public static void AddAuthenticationExtension(this IServiceCollection services, JwtSettings jwtSettings)
        {
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            if (context.HttpContext.Request.Query.ContainsKey("token"))
                            {
                                context.Token = context.HttpContext.Request.Query["token"];
                            }
                            else if (context.HttpContext.Request.Cookies.ContainsKey("token"))
                            {
                                context.Token = context.HttpContext.Request.Cookies["token"];
                            }

                            return Task.CompletedTask;
                        }
                    };
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Secret)),
                        ValidateAudience = false,
                        ValidIssuer = jwtSettings.Issuer,
                        ClockSkew = TimeSpan.Zero,
                        ValidateActor = false
                    };
                });
        }
    }
}
