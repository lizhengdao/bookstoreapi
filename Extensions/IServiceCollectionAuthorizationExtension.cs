﻿using BooksApi.Constants;
using Microsoft.Extensions.DependencyInjection;

namespace BooksApi.Extensions
{
    public static class IServiceCollectionAuthorizationExtension
    {
        public static void AddAuthorizationExtension(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(AuthorizationPolicies.AdminPolicy, policy =>
                {
                    policy.RequireRole(UserRoles.Admin);
                });
                options.AddPolicy(AuthorizationPolicies.CustomerPolicy, policy =>
                {
                    policy.RequireRole(UserRoles.Customer);
                });
            });
        }
    }
}
