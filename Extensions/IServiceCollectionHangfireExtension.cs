﻿using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BooksApi.Extensions
{
    public static class IServiceCollectionHangfireExtension
    {
        public static void AddHangfireExtension(this IServiceCollection services, string connectionString)
        {
            services.AddHangfire(config =>
            {
                config.UseSimpleAssemblyNameTypeSerializer()
                             .UseRecommendedSerializerSettings()
                             .UseSqlServerStorage(connectionString, new Hangfire.SqlServer.SqlServerStorageOptions()
                             {
                                 CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                                 SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                                 QueuePollInterval = TimeSpan.Zero,
                                 UseRecommendedIsolationLevel = true,
                                 DisableGlobalLocks = true
                             });
            });

            services.AddHangfireServer();
        }
    }
}
