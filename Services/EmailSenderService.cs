﻿using BooksApi.Models;
using BooksApi.Utilites;
using MailKit.Net.Smtp;
using MimeKit;
using System.Threading.Tasks;

namespace BooksApi.Services
{
    public class EmailSenderService
    {
        private readonly EmailMetadata emailMetadata;
        private readonly EmailMessage emailMessage;
        private readonly MimeMessage mimeMessage;
        private readonly RazorViewToStringRenderer razorViewToStringRenderer;

        public EmailSenderService(
            EmailMetadata emailMetadata,
            EmailMessage emailMessage,
            MimeMessage mimeMessage,
            RazorViewToStringRenderer razorViewToStringRenderer)
        {
            this.emailMetadata = emailMetadata;
            this.emailMessage = emailMessage;
            this.mimeMessage = mimeMessage;
            this.razorViewToStringRenderer = razorViewToStringRenderer;
        }
        public void SendToAdmins()
        {

        }

        public async Task SendToCustomerAsync(CustomerWithOrders customerWithOrder)
        {
            emailMessage.Sender = new MailboxAddress("Self", emailMetadata.Sender);
            emailMessage.Reciever = new MailboxAddress("Self", customerWithOrder.Customer.Email);
            emailMessage.Subject = "Cart Items";
            emailMessage.Content = await razorViewToStringRenderer.RenderViewToStringAsync("/HtmlTemplates/CustomerCartEmail.cshtml", customerWithOrder);

            var mimeMessage = CreateMimeMessageFromEmailMessage(emailMessage);

            using (SmtpClient smtpClient = new SmtpClient())
            {
                smtpClient.Connect(emailMetadata.SmtpServer,
                emailMetadata.Port, true);
                smtpClient.Authenticate(emailMetadata.Username,
                emailMetadata.Password);
                smtpClient.Send(mimeMessage);
                smtpClient.Disconnect(true);
            }
        }
        private MimeMessage CreateMimeMessageFromEmailMessage(EmailMessage message)
        {
            mimeMessage.From.Add(message.Sender);
            mimeMessage.To.Add(message.Reciever);
            mimeMessage.Subject = message.Subject;
            mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            { Text = message.Content };

            return mimeMessage;
        }
    }
}
