﻿using BooksApi.Data;
using BooksApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace BooksApi.Services
{
    public class AuthorBooksService
    {
        private readonly BooksApiContext context;

        public AuthorBooksService(BooksApiContext context)
        {
            this.context = context;
        }
        // GET by id
        public async Task<AuthorBook> GetByIdAsync(int bookId)
        {
            return await context.AuthorBooks
                .Include(ba => ba.Author)
                .Include(ba => ba.Book)
                .AsNoTracking()
                .FirstOrDefaultAsync(ba => ba.BookId == bookId);
        }
        // ADD
        public async Task<AuthorBook> AddAsync(Author author, Book book)
        {
            AuthorBook authorBook = new AuthorBook
            {
                AuthorId = author.Id,
                BookId = book.Id
            };

            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var bookAuthor = await context.AuthorBooks.AddAsync(authorBook);

            await context.SaveChangesAsync();

            var response = await context.AuthorBooks
                .Include(ab => ab.Author)
                .Include(ab => ab.Book)
                .AsNoTracking()
                .Where(ab => ab.Id == bookAuthor.Entity.Id)
                .FirstOrDefaultAsync();

            scope.Complete();

            return response;
        }
        // DELETE
        public async Task DeleteAsync(int id)
        {
            var bookAuthor = await context.AuthorBooks.FindAsync(id);

            context.AuthorBooks.Remove(bookAuthor);

            await context.SaveChangesAsync();
        }
    }
}
