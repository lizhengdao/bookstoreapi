﻿using AutoMapper;
using BooksApi.Constants;
using BooksApi.Data;
using BooksApi.Enums;
using BooksApi.Exceptions;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Responses.Book;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace BooksApi.Services
{
    public class UserService
    {
        private readonly BooksApiContext context;
        private readonly IMapper mapper;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly ImageService imageService;

        public UserService(
            BooksApiContext context,
            IMapper mapper,
            UserManager<ApplicationUser> userManager,
            ImageService imageService)
        {
            this.context = context;
            this.mapper = mapper;
            this.userManager = userManager;
            this.imageService = imageService;
        }
        // Get all
        public async Task<IList<GetUserResponse>> GetAsync()
        {
            var res = await context.ApplicationUsers
                .AsNoTracking()
                .ToListAsync();

            return mapper.Map<IList<GetUserResponse>>(res);
        }
        // Get by id
        public async Task<GetUserResponse> GetByIdAsync(int id)
        {
            var res = await context.ApplicationUsers
                .AsNoTracking()
                .FirstOrDefaultAsync(u => u.Id == id);

            return mapper.Map<GetUserResponse>(res);
        }
        // Get favorites
        public async Task<IEnumerable<GetBookResponse>> GetUserFavoritesAsync(int userId)
        {
            var res = await context.Favorites
                .Where(f => f.UserId == userId)
                .Select(f => f.Book)
                .AsNoTracking()
                .ToListAsync();

            return mapper.Map<IEnumerable<GetBookResponse>>(res);
        }
        // Add stripe user id token
        public async Task AddStripeIdToUserAsync(ApplicationUser user, string stripeUserId)
        {
            user.StripeCustomerId = stripeUserId;
            context.Users.Update(user);

            await context.SaveChangesAsync();
        }
        // Add new user
        public async Task<ApplicationUser> CreateUser(PostRegisterUserRequest value)
        {
            var user = mapper.Map<ApplicationUser>(value);

            var addUser = await userManager.CreateAsync(user, value.Password);

            if (!addUser.Succeeded && addUser.Errors.Any(e => e.Code == "DuplicateEmail"))
            {
                throw new ServiceException(ServiceExceptionMessages.UserEmailInUse);
            }
            if (!addUser.Succeeded && addUser.Errors.Any(e => e.Code == "DuplicateUsername"))
            {
                throw new ServiceException(ServiceExceptionMessages.UserUsernameInUse);
            }

            await userManager.AddToRoleAsync(user, UserRoles.Customer);
            await userManager.AddClaimAsync(user, new Claim(AccessTokenClaims.UserEmail, user.Email));

            return user;

        }
        //Update user avatar
        // image file
        public async Task UpdateAvatarAsync(int id, IFormFile newAvatar)
        {
            var user = await context.ApplicationUsers.FindAsync(id);

            if (user.Avatar != null)
            {
                imageService.ReplaceImage(newAvatar, user.Avatar);
            }
            else
            {
                var avatarPath = await imageService.UploadImageAsync(newAvatar, ImageStorageDirectory.Avatars);

                if (avatarPath == null)
                {
                    throw new ServiceException(ServiceExceptionMessages.ImageUploadFailed);
                }

                user.Avatar = avatarPath;
                await context.SaveChangesAsync();
            }
        }
        // Update user avatar
        // Json
        public async Task UpdateAvatarAsync(int id, PostAvatarRequest avatar)
        {
            var user = await context.ApplicationUsers.FindAsync(id);

            if (user.Avatar != null)
            {
                imageService.ReplaceImage(avatar.Avatar, user.Avatar);
            }
            else
            {
                var avatarPath = await imageService.UploadImageAsync(avatar.Avatar, ImageStorageDirectory.Avatars);

                if (avatarPath == null)
                {
                    throw new ServiceException(ServiceExceptionMessages.ImageUploadFailed);
                }

                user.Avatar = avatarPath;
                await context.SaveChangesAsync();
            }
        }

        // Update
        public async Task UpdateAsync(int id, PutUserRequest userToUpdate)
        {
            var user = await context.ApplicationUsers.FindAsync(id);

            mapper.Map(userToUpdate, user);

            var res = await userManager.UpdateAsync(user);

            if (!res.Succeeded)
            {
                throw new ServiceException(ServiceExceptionMessages.UserUpdateFailed);
            }
        }
        // Partial update

        // Delete
        public async Task<int> DeleteAsync(int id)
        {
            // Soft Delete
            return 1;
        }
        // Delete avatar
        public async Task DeleteAvatarAsync(int id)
        {
            ApplicationUser user = context.ApplicationUsers.Find(id);

            if (user.Avatar == null)
            {
                throw new ServiceException(ServiceExceptionMessages.ImageNotFound);
            }

            imageService.DeleteAnImage(user.Avatar);

            user.Avatar = null;
            await userManager.UpdateAsync(user);
        }
    }
}
