﻿using AutoMapper;
using BooksApi.Constants;
using BooksApi.Data;
using BooksApi.Enums;
using BooksApi.Exceptions;
using BooksApi.Models;
using BooksApi.Requests;
using BooksApi.Responses;
using BooksApi.Responses.Book;
using LazyCache;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace BooksApi.Services
{
    public class BookService
    {
        private readonly BooksApiContext context;
        private readonly IMapper mapper;
        private readonly QueryService queryService;
        private readonly ImageService imageUploadService;
        private readonly IAppCache appCache;
        private readonly AuthorService authorService;
        private readonly AuthorBooksService authorBooksService;

        public BookService(
            BooksApiContext context,
            IMapper mapper,
            QueryService queryService,
            ImageService imageUploadService,
            IAppCache appCache,
            AuthorService authorService,
            AuthorBooksService authorBooksService)
        {
            this.context = context;
            this.mapper = mapper;
            this.queryService = queryService;
            this.imageUploadService = imageUploadService;
            this.appCache = appCache;
            this.authorService = authorService;
            this.authorBooksService = authorBooksService;
        }
        // Get all
        public async Task<PagedResponse<GetBookResponse>> GetAsync(
            PaginationQuery pageQuery,
            SortQuery sortQuery,
            FilterQuery filterQuery)
        {
            var query = context.Books
                .Include(b => b.AuthorBooks)
                .ThenInclude(ab => ab.Author)
                .Where(b => b.Isbn.StartsWith(filterQuery.Filter) || b.Title.StartsWith(filterQuery.Filter))
                .AsQueryable();

            var count = await query
                .AsNoTracking()
                .CountAsync();

            var totalPages = (int)Math.Ceiling(count / (double)pageQuery.PerPage);

            if (pageQuery.Page > totalPages)
            {
                return null;
            }

            if (sortQuery.SortBy == BookSortingType.Asc)
            {
                query = queryService.OrderByAscendingOrder(sortQuery.OrderBy, query);
            }
            else if (sortQuery.SortBy == BookSortingType.Desc)
            {
                query = queryService.OrderByDescendingOrder(sortQuery.OrderBy, query);
            }

            var skip = (pageQuery.Page - 1) * pageQuery.PerPage;

            var books = await query
               .Skip(skip).Take(pageQuery.PerPage)
               .AsNoTracking()
               .ToListAsync();

            var response = mapper.Map<IEnumerable<GetBookResponse>>(books);

            return new PagedResponse<GetBookResponse>(
                response,
                pageQuery.Page,
                response.ToList().Count,
                pageQuery.PerPage,
                totalPages,
                sortQuery.SortBy,
                sortQuery.OrderBy);
        }
        // Get by id
        public async Task<GetBookResponse> GetByIdAsync(int bookId)
        {
            var book = await context.Books
                 .AsNoTracking()
                 .Include(b => b.AuthorBooks)
                 .ThenInclude(ab => ab.Author)
                 .Where(b => b.Id.Equals(bookId))
                 .FirstOrDefaultAsync();

            return mapper.Map<GetBookResponse>(book);
        }
        //Add new
        public async Task<GetBookResponse> AddAsync(PostBookRequest newBook)
        {
            var book = mapper.Map<Book>(newBook);
            var author = mapper.Map<Author>(newBook.Author);

            if (await HasIsbnAsync(book.Isbn))
            {
                throw new ServiceException(ServiceExceptionMessages.BookExists);
            }
            if (newBook.Image != null)
            {
                string bookImagePath = await imageUploadService.UploadImageAsync(book.Image, ImageStorageDirectory.Books);

                book.Image = bookImagePath;
            }

            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            if (!await authorService.HasAuthorAsync(author))
            {
                await authorService.AddAsync(author);
            }
            else
            {
                author = await authorService.GetAuthorAsync(author);
            }

            await context.Books.AddAsync(book);
            await context.SaveChangesAsync();

            var authorBook = await authorBooksService.AddAsync(author, book);

            scope.Complete();

            return mapper.Map<GetBookResponse>(authorBook);
        }
        // Update
        public async Task UpdateAsync(int id, PutBookRequest newBook)
        {
            var book = await context.Books.FindAsync(id);

            if (newBook.Image != null)
            {
                imageUploadService.ReplaceImage(newBook.Image, book.Image);
            }

            mapper.Map(newBook, book);

            await context.SaveChangesAsync();
        }
        // 
        private bool HasPatchDocImageOperations(JsonPatchDocument patchDoc)
        {
            return patchDoc.Operations.Any(operation =>
            {
                return (
                operation.op.ToLower() == OperationType.Add.ToString().ToLower() ||
                operation.op.ToLower() == OperationType.Replace.ToString().ToLower() ||
                operation.op.ToLower() == OperationType.Remove.ToString().ToLower()) &&
                operation.path.ToLower() == "image";
            });
        }
        // Patch
        public async Task PatchAsync(int id, JsonPatchDocument patchDoc)
        {
            var book = await context.Books.FindAsync(id);

            if (HasPatchDocImageOperations(patchDoc))
            {
                var add = patchDoc.Operations.Find(op => op.op.ToLower() == OperationType.Add.ToString().ToLower());
                var remove = patchDoc.Operations.Find(op => op.op.ToLower() == OperationType.Remove.ToString().ToLower());
                var replace = patchDoc.Operations.Find(op => op.op.ToLower() == OperationType.Replace.ToString().ToLower());

                if (add != null && add.path == "image")
                {
                    var imagePath = await imageUploadService.UploadImageAsync((string)add.value, ImageStorageDirectory.Books);
                    book.Image = imagePath;
                }
                if (remove != null && remove.path == "image")
                {
                    imageUploadService.DeleteAnImage(book.Image);
                }
                if (replace != null && replace.path == "image")
                {
                    imageUploadService.ReplaceImage((string)replace.value, book.Image);
                }
            }

            patchDoc.ApplyTo(book);

            await context.SaveChangesAsync();
        }
        // Delete
        public async Task DeleteAsync(int id)
        {
            var authorBook = await context
                .AuthorBooks
                .FirstOrDefaultAsync(ab => ab.BookId.Equals(id));

            await authorBooksService.DeleteAsync(authorBook.Id);
        }
        // Check if book exists
        public async Task<bool> HasIsbnAsync(string isbn)
        {
            return await context.Books.AsNoTracking().AnyAsync(b => b.Isbn.Equals(isbn));
        }
    }
}
