﻿using BooksApi.Constants;
using BooksApi.Data;
using BooksApi.Exceptions;
using BooksApi.Models;
using BooksApi.Requests.Checkout;
using BooksApi.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Stripe;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;

namespace BooksApi.Services
{
    public class PaymentService
    {
        private readonly StripeSettings stripeSettings;
        private readonly CartService cartService;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly CustomerService customerService;
        private readonly ChargeService chargeService;
        private readonly PaymentIntentService paymentIntentService;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly UserService userService;
        private readonly BooksApiContext context;
        private readonly BooksApiOrderService booksApiOrderService;
        private readonly BookService bookService;
        private readonly BooksApiCouponService couponService;

        public PaymentService(
            StripeSettings stripeSettings,
            CartService cartService,
            UserManager<ApplicationUser> userManager,
            CustomerService customerService,
            ChargeService chargeService,
            PaymentIntentService paymentIntentService,
            IHttpContextAccessor httpContextAccessor,
            UserService userService,
            BooksApiContext context,
            BooksApiOrderService booksApiOrderService,
            BookService bookService,
            BooksApiCouponService couponService)
        {
            this.stripeSettings = stripeSettings;
            this.cartService = cartService;
            this.userManager = userManager;
            this.customerService = customerService;
            this.chargeService = chargeService;
            this.paymentIntentService = paymentIntentService;
            this.httpContextAccessor = httpContextAccessor;
            this.userService = userService;
            this.context = context;
            this.booksApiOrderService = booksApiOrderService;
            this.bookService = bookService;
            this.couponService = couponService;
            StripeConfiguration.ApiKey = stripeSettings.SecretKey;
        }
        // Create payment
        private async Task<PaymentIntent> CreatePayment(decimal total, int cartId, string userId)
        {
            var options = new PaymentIntentCreateOptions
            {
                Amount = (int)(total * 100),
                Currency = stripeSettings.Currency,
                PaymentMethodTypes = new List<string> { "card" },
                Metadata = new Dictionary<string, string>
                {
                    { "CartId", cartId.ToString()},
                    { "UserId", userId}
                }
            };

            return await paymentIntentService.CreateAsync(options);
        }
        // Create charge
        private async Task<Charge> CreateChargeAsync(PaymentIntent paymentIntent, string token)
        {
            var charge = new ChargeCreateOptions
            {
                Amount = paymentIntent.Amount,
                Currency = paymentIntent.Currency,
                Metadata = paymentIntent.Metadata,
                Description = "Book charge test",
                Source = token
            };

            return await chargeService.CreateAsync(charge);
        }
        // Payment
        public async Task<GetStripeResponse> Payment(int cartId)
        {
            var cart = await cartService.GetByIdAsync(cartId);

            if (cart == null)
            {
                throw new ServiceException(ServiceExceptionMessages.CartNotFound);
            }

            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var total = cartService.CalculateTotalForACart(cart.Id);
            var coupon = cart.Coupon;

            if (coupon != null && coupon.ValidNum > 0)
            {
                total = (1 - coupon.ProcentOff / 100) * total;
                //decimal procentageOff = (decimal)((int)(total * 100) * coupon.ProcentOff / 100) / 100;

                //total -= procentageOff;

                await couponService.DecreaseCouponValidNumberAsync(coupon.Id);
            }

            var userId = httpContextAccessor.HttpContext.User.FindFirstValue(AccessTokenClaims.UserId);

            var paymentIntent = await CreatePayment(total, cart.Id, userId);

            await cartService.AddStripePaymentIntentId(cart.Id, paymentIntent.Id);

            scope.Complete();

            return new GetStripeResponse()
            {
                ClientSecret = paymentIntent.ClientSecret
            };
        }
        // Confirm a payment
        public async Task<GetPaymentResponse> ConfirmPaymentAsync(int cartId, PostPaymentRequest paymentRequest)
        {
            var cart = await context
                .Carts
                .Include(c => c.CartBooks)
                .ThenInclude(cb => cb.Book)
                .FirstOrDefaultAsync(c => c.Id.Equals(cartId));

            if (cart == null)
            {
                throw new ServiceException(ServiceExceptionMessages.CartNotFound);
            }

            cart.CartBooks.ToList().ForEach(cb =>
            {
                if (cb.Book.Stock < cb.Quantity)
                {
                    throw new ServiceException(ServiceExceptionMessages.BookOutOfStock);
                }
            });

            var paymentIntent = await paymentIntentService.GetAsync(cart.StripePaymentId);

            var charge = await CreateChargeAsync(paymentIntent, paymentRequest.Token);

            if (charge.Status.Equals("failed"))
            {
                return new GetPaymentResponse
                {
                    Message = $"Payment failed because {charge.FailureMessage}",
                    Order = null
                };
            }

            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var order = await CreateOrderAsync(cart.Id, cart.UserId, charge.Amount);

            await cartService.UpdateCartStatusAsync(cart);

            cart.CartBooks.ToList().ForEach(cb =>
            {
                cb.Book.Stock -= cb.Quantity;
            });

            await context.SaveChangesAsync();

            scope.Complete();

            return new GetPaymentResponse
            {
                Message = "Payment confirmed",
                Order = order
            };
        }
        // Add order
        public async Task<GetOrderResponse> CreateOrderAsync(int cartId, int userId, decimal total)
        {
            var order = new Models.Order
            {
                CartId = cartId,
                UserId = userId,
                Total = total
            };

            return await booksApiOrderService.AddAsync(order);
        }
    }
}
