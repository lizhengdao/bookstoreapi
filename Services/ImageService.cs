﻿using BooksApi.Constants;
using BooksApi.Enums;
using BooksApi.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BooksApi.Services
{
    public class ImageService
    {
        public ImageService(IWebHostEnvironment webHostEnvironment)
        {
            StorageDirectory = Path.Combine(webHostEnvironment.ContentRootPath, "images");
            BookImagesDir = Path.Combine(StorageDirectory, "books");
            AvatarImagesDir = Path.Combine(StorageDirectory, "users");
        }

        public string StorageDirectory { get; private set; }

        public string BookImagesDir { get; private set; }

        public string AvatarImagesDir { get; private set; }

        private bool IsAValidImage(IFormFile image)
        {
            return image.Length > 3096 && !IsImage(image) ? false : true;
        }

        private bool IsImage(IFormFile image)
        {
            return image.ContentType == "image/jpg" || image.ContentType == "image/jpeg" || image.ContentType == "image/png";
        }

        private string NormalizeBase64String(string base64String)
        {
            return base64String.Split(",").ElementAt(1);
        }

        private bool IsAValidBase64String(string base64String)
        {
            return Regex.IsMatch(NormalizeBase64String(base64String), @"^[A-Za-z0-9\+\/=]+$");
        }

        private string GetDirectoryPathForDestination(ImageStorageDirectory destination)
        {
            return destination switch
            {
                ImageStorageDirectory.Avatars => AvatarImagesDir,
                ImageStorageDirectory.Books => BookImagesDir,
                _ => StorageDirectory
            };
        }

        private string ConstructImagePathForAFile(IFormFile formFile, ImageStorageDirectory destination)
        {
            var directory = GetDirectoryPathForDestination(destination);

            string imageName = Guid.NewGuid().ToString();
            string imageType = "." + formFile.ContentType.Split("/").ElementAt(1);

            return Path.Combine(directory, imageName + imageType);
        }

        private string ConstructImagePathForABase64StringImage(string base64StringImage, ImageStorageDirectory destination)
        {
            var directory = GetDirectoryPathForDestination(destination);

            string imageName = Guid.NewGuid().ToString();
            string imageType = "." + base64StringImage.Split(":").ElementAt(1).Split("/").ElementAt(1).Split(";").ElementAt(0);

            return Path.Combine(directory, imageName + imageType);
        }

        public void DeleteAnImage(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (DirectoryNotFoundException e)
            {
                throw new ServiceException(e.Message);
            }
        }

        public async Task<string> UploadImageAsync(IFormFile formFile, ImageStorageDirectory destination)
        {
            if (!IsAValidImage(formFile))
            {
                throw new ServiceException(ServiceExceptionMessages.ImageNotValid);
            }

            string imagePath = ConstructImagePathForAFile(formFile, destination);
            string directory = GetDirectoryPathForDestination(destination);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (var stream = File.Create(imagePath))
            {
                await formFile.CopyToAsync(stream);
            };

            return imagePath;
        }

        public void ReplaceImage(IFormFile sourceFile, string destinationPath)
        {
            if (!IsAValidImage(sourceFile))
            {
                throw new ServiceException(ServiceExceptionMessages.ImageNotValid);
            }
            if (!File.Exists(destinationPath))
            {
                throw new ServiceException(ServiceExceptionMessages.ImageNotFound);
            }

            DeleteAnImage(destinationPath);

            using (var stream = File.Create(destinationPath))
            {
                sourceFile.CopyTo(stream);
            };
        }

        public async Task<string> UploadImageAsync(string base64Image, ImageStorageDirectory destination)
        {

            if (!IsAValidBase64String(base64Image))
            {
                throw new ServiceException(ServiceExceptionMessages.ImageNotValidBase64String);
            }

            var directory = GetDirectoryPathForDestination(destination);
            var imagePath = ConstructImagePathForABase64StringImage(base64Image, destination);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            await File.WriteAllBytesAsync(imagePath, Convert.FromBase64String(NormalizeBase64String(base64Image)));

            return imagePath;
        }

        public void ReplaceImage(string base64StringImage, string destinationPath)
        {
            if (!IsAValidBase64String(base64StringImage))
            {
                throw new ServiceException(ServiceExceptionMessages.ImageNotValidBase64String);
            }
            if (!File.Exists(destinationPath))
            {
                throw new ServiceException(ServiceExceptionMessages.ImageNotFound);
            }

            DeleteAnImage(destinationPath);
            // Write new image to path
            File.WriteAllBytes(destinationPath, Convert.FromBase64String(NormalizeBase64String(base64StringImage)));
        }

        public string ConvertUserAvatarToBase64String(string pathToImage)
        {
            if (!File.Exists(pathToImage))
            {
                return null;
            }

            byte[] file = File.ReadAllBytes(pathToImage);
            string imageType = pathToImage.Split(".").ElementAt(1);

            return $"data:image/{imageType};base64," + Convert.ToBase64String(file);
        }
    }
}
