﻿namespace BooksApi.Constants
{
    public static class CacheKeys
    {
        public const string OrderServiceGetStatsAsync = "BooksApiOrderService-GetStatsAsync";
    }
}
