﻿namespace BooksApi.Constants
{
    public static class AccessTokenClaims
    {
        public const string UserId = "uid";

        public const string SessionId = "sid";

        public const string UserEmail = "uem";
    }
}
