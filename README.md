## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
BookStoreApi is a REST API for an imaginary shop that sells books.    
Details provided in Swagger documentation.

## Technologies
Project was created with:
* ASP .Net Core

## Setup
To run this project you will need:
* [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)
* [.NET Core SDK 3.1 or later](https://dotnet.microsoft.com/download)

Start it from project directory with command
```
$ dotnet run 
```

Alternatively open projects solution in Visual Studio and run it from there.
