﻿namespace BooksApi.Models
{
    public class FilterQuery
    {
        public FilterQuery()
        {
            Filter = "";
        }
        public FilterQuery(string filter)
        {
            Filter = filter;
        }

        public string Filter { get; set; }
    }
}
