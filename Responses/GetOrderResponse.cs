﻿using System;
using System.Collections.Generic;

namespace BooksApi.Responses
{
    public class GetOrderResponse
    {
        public int Id { get; set; }

        public IEnumerable<GetCartBookResponse> CartItems { get; set; }

        public double Total { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
