﻿using BooksApi.Responses.Book;

namespace BooksApi.Responses
{
    public class GetFavoriteResponse
    {
        public int Id { get; set; }

        public GetUserResponse User { get; set; }

        public GetBookResponse Book { get; set; }
    }
}
