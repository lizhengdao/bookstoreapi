﻿namespace BooksApi.Responses
{
    public class GetStripeResponse
    {
        public string ClientSecret { get; set; }
    }
}
