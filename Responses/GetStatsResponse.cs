﻿namespace BooksApi.Responses
{
    public class GetStatsResponse
    {
        public int TotalOrders { get; set; }

        public decimal TotalValue { get; set; }

        public int OrdersInPastMonth { get; set; }

        public int OrdersInPastSixMonths { get; set; }

        public int ConfirmedOrders { get; set; }

        public int UnconfirmedOrders { get; set; }
    }
}
