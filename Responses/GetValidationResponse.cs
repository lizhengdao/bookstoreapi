﻿using BooksApi.Models;
using System.Collections.Generic;

namespace BooksApi.Responses
{
    public class GetValidationResponse
    {
        public string Message { get; set; }

        public IEnumerable<IEnumerable<ValidationMessage>> Errors { get; set; }

        public GetValidationResponse(IEnumerable<IEnumerable<ValidationMessage>> errors, string message = "Validation Failed")
        {
            Message = message;
            Errors = errors;
        }
    }
}
