﻿namespace BooksApi.Responses
{
    public class GetTokenResponse
    {
        public string Token { get; set; }
    }
}
