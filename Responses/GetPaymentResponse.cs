﻿namespace BooksApi.Responses
{
    public class GetPaymentResponse
    {
        public string Message { get; set; }

        public GetOrderResponse Order { get; set; }
    }
}
