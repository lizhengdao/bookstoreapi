﻿using BooksApi.Responses.Book;
using System;

namespace BooksApi.Responses
{
    public class GetCartBookResponse
    {
        public int Id { get; set; }

        public GetBookResponse Book { get; set; }

        public int Quantity { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
