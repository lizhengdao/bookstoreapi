﻿using System;

namespace BooksApi.Responses
{
    public class GetSessionResponse
    {
        public int Id { get; set; }

        public GetUserResponse User { get; set; }

        public string Client { get; set; }

        public string RefreshToken { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
