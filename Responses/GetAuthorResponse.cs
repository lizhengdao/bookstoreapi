﻿namespace BooksApi.Responses
{
    public class GetAuthorResponse
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
