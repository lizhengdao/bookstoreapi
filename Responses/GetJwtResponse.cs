﻿namespace BooksApi.Responses
{
    public class GetJwtResponse
    {
        public string Token { get; set; }

        public string RefreshToken { get; set; }
    }
}
