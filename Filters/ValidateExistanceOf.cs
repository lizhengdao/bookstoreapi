﻿using BooksApi.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace BooksApi.Filters
{
    public class ValidateExistanceOf<T> : IAsyncActionFilter where T : class
    {
        private readonly BooksApiContext dbContext;

        public ValidateExistanceOf(BooksApiContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var id = Convert.ToInt32(context.HttpContext.Request.RouteValues["id"]);

            if (await dbContext.Set<T>().FindAsync(id) == null)
            {
                context.Result = new NotFoundResult();
                return;
            }

            await next();
        }
    }
}
