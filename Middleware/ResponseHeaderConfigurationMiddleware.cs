﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace BooksApi.Middleware
{
    public class ResponseHeaderConfigurationMiddleware
    {
        private readonly RequestDelegate next;

        public ResponseHeaderConfigurationMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            context.Response.GetTypedHeaders().CacheControl =
              new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
              {
                  Public = true,
                  MaxAge = TimeSpan.FromSeconds(30)
              };

            context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Expires] = DateTime.Now.AddSeconds(30).ToString();

            await next(context);
        }
    }
}
