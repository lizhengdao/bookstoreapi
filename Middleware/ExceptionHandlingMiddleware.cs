﻿using Microsoft.AspNetCore.Http;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BooksApi.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(Exception e, HttpContext context)
        {
            if (e is SqlException exception)
            {
                Console.WriteLine(exception.Message);
                return;
            }

            await next(context);
        }
    }
}
